# Aurora theme for Firefox view-source pages

Perks of using this theme include: 

- Your eyes not burning upon viewing the source of a webpage. (This is a dark-ish theme.)
- A pleasant looking theme. 

# Licensing and donation information:

https://c9x3.neocities.org/
